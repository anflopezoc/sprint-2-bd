const config = require('../config/config').module;

const swaggerOptions = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "Sprint Project Delilah Restó with DB-Sprint Project 2 Acámica",
            version: "2.0.0",
            description: "API for orders of Delilah Restó users made with Node JS and MariaDB"
        },
        servers: [
            {
                url: "http://localhost:3000",
                description: "Local server"
            }
        ],
        components: {
            securitySchemes: {
                bearerAuth: {
                    type: "http",
                    scheme: "bearer",
                    bearerFormat: "JWT"
                }
            }
        },
        security: [

        ]
    },
    apis: ["./src/routes/*.js", "./components.yaml"]
};

module.exports = swaggerOptions;
