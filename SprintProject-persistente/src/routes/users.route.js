const express = require('express');
const router = express.Router();
const controller = require('../controllers/users.controller');
const middlewareAdmin = require('../middlewares/adminvalidator.middleware').adminvalidator;
const middlewareUser = require('../middlewares/uservalidator.middleware')

//Middleware
router.use('/',middlewareAdmin)


//Routes

router.get('/allusers',  controller.allUsers);

router.post('/newuser', middlewareUser.repeatedemail,controller.createUser);

router.put('/userupdate/:id', controller.userUpdate);

router.delete('/userinactive/:id', controller.userInactivate)

module.exports = router;