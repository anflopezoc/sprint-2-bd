const express = require('express');
const router = express.Router();
const controller = require('../controllers/addresses.controllers')

router.get('/alladdresses', controller.allAddresses);

router.post('/newaddress', controller.newAddresses);

router.put('/addressupdate/:id', controller.adressUpdate);

router.delete('/deleteaddress/:id', controller.adressDelete);

module.exports = router