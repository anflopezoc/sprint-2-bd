# Prueba 2 - Reduce

Esta es una REST API que permite traer un JSON proveniente de los registos de usuarios de un archivo .csv que está en la carpeta src/DB/ del repositorio en la misma y está basado en el requerimiento de la prueba 2.

## ¿Qué permite hacer esta REST API?
Permite traer con el método **GET** un endpoint que trae un JSON agrupado de acuerdo a la petición de la prueba 2.

## ¿Qué tecnologías tiene esta REST API?

Esta REST API está hecha en base al entorno NodeJS del lenguaje Javascript, que, dentro de la misma, utiliza las siguientes librerías:

 1. **ExpressJS** : el framework por excelencia de Javascript y NodeJS
 2. **Node-fetch**: librería de uso asíncrono para comunicación con otras API.
 3. **swagger-jsdoc & swagger-ui-express:** dos herramientas que en conjunto posibilitan la documentación de la REST API (más adelante se explica cómo funciona) para evitar crear un archivo HTML.
 4. **Csvtojson:** una librería que permite leer y convertir en JSON los registros de un archivo csv.

## ¿Cómo inicio la REST API?

Ya que estás en este repositorio de GIT HUB, puedes descargar el archivo comprimido de este repositorio y guardarlo en tu computadora para probarlo.

Debes entrar a la terminal, sea desde tu GitBash, Powershel o desde el mismo editor de texto; luego de esto, ubicarte en la carpeta Parte2 del repositorio para luego instalar las librerías necesarias para funcionar.

Es necesario antes tener instalado NodeJS en el computador de prueba y con todo su paquete de instalación de librerías. Si cuenta con esto, debes desde la terminar situada en la carpeta de la REST API iniciar con este comando:

    npm install

Luego de esto, verás que se instalará la carpeta node_modules con todas las librerías necesarias para iniciar la REST API. Luego, inicias el servidor con el siguiente comando:
 
    node src/index.js 
     
o con nodemon si prefieres, a través de:
 
    nodemon src/index.js 
     
luego de esto, debes verificar que en la terminal aparezca ***Listen port 3000*** para confirmar que ha iniciado el servidor de la REST API.

Por último, debes ingresar a el siguiente enlace:
[http://localhost:3000/swagger/](http://localhost:3000/swagger/)

## ¿Cómo funciona la REST API?

En la documentación de Swagger, verás que tiene un tag llamado GET, en él verás dos endpoints GET donde podrás ver el backend de la REST API.

Para probar este endpoint debes darle click encima y luego darle al botón **Try it out** que está en el costado lateral derecho del apartado de parameters. Diferente a la prueba 1, aquí no se es requerido ningún valor por parameters, por lo que puede dar automáticamente al botón azúl inferior llamado ***Execute***, luego de eso, verá en el apartado de Responses una respuesta, que principalmente debe fijarse en la parte inferior donde aparece el **Response body** donde verá el JSON con su respuesta.

Dado lo anterior, puede encontrar:

 - [GET. organizaciones:](http://localhost:3000/swagger/#/GET/get_get_organizaciones) este endpoint solamente devuelve un resumen de los datos suministrados desde el archivo csv en el **Response body**  con un JSON agrupado de acuerdo al orden:
	 1. Organización
	 2. Usuario
	 3. Rol

	**Nota**: vale aclarar que obtendrá un JSON con un dato demás, debido al ingreso de un registro más para verificar que  la función reduce realizada lograba anidar correctamente los roles de un usuario con más de uno o dos registro.

Has finalizado la revisión de la Prueba 2, gracias por leerme, espero todo haya salido con éxito.

Resuelto por:
Andrés Felipe López Ocho
  Backend Developer Junior 
	 - email: anflopezoc@gmail.com
	 - 3144499986
	 - [LinkedIn](https://www.linkedin.com/in/anflopezoc/)
