const express = require('express');
const app = express();
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

//Swagger
const swaggerOptions = require('./utils/swaggerOptions');
const swaggerSpecs = swaggerJsDoc(swaggerOptions);
app.use('/swagger', swaggerUI.serve, swaggerUI.setup(swaggerSpecs));


//Middleware
app.use(express.json());


//Routes
app.use('/get', require('./Routes/api-rout'))


//Server
app.listen(3000, console.log('Listen port 3000'))
