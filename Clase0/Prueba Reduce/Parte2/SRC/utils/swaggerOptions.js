const swaggerOptions = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "Parte 2-Prueba Backend IPCOM",
            version: "1.0.0",
            description: "Prueba sobre un JSON desde datos en formato CSV"
        },
        servers: [
            {
                url: "http://localhost:3000",
                description: "Local server"
            }
        ],
        components: {

        },
        security: [

        ]
    },
    apis: ["./src/routes/*.js"]
};

module.exports = swaggerOptions;
