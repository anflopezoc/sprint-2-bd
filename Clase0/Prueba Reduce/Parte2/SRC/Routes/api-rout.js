
const express = require('express');
const router = express.Router();
const csvJson = require('csvtojson');

//Función reduce para devolver petición
const groupOrg = (array, org,user,rol) => {
      return array.reduce((result, Value) => {
        if (!result[Value[org]]) {
          result[Value[org]] = [];
        }
        const resultFind = result[Value[org]].find( u => u[user] === Value[user])
        if(resultFind){
          if(typeof resultFind[rol] === 'object'){
            if(!resultFind.rol.some(u => u === Value[rol])){
              resultFind.rol.push(Value[rol])
            }
          } else resultFind.rol = [resultFind.rol, Value[rol]]
        }else result[Value[org]].push(Value)
        return result;
      },{}); 
    };

//Trae un JSON con los valores asignados. 
/**
 * @swagger
 * /get/organizaciones:
 *      get:    
 *          summary: Devuelve un JSOn dividido en 2 tipos de organizaciones
 *          description: A través del Params, se ingresa el numero de dias que desea verificar y el clientID del usuario para traer su información
 *          tags: [GET]   
 *          responses:
 *                  200:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 *                  400:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */

 router.get('/organizaciones', async (req, res) => {
    const arr= await csvJson().fromFile('./src/DB/data.csv');
    const prueba = groupOrg(arr,'organizacion','usuario','rol')
    res.json(prueba)
 });

module.exports = router
