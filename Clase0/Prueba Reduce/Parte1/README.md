
# Prueba 1 - Reduce

Esta es una REST API que le permite a un cliente realizar análisis de las ventas de su call center. Esta trae información de la  base de datos suministrada por  una  [API](https://apirecruit-gjvkhl2c6a-uc.a.run.app/compras/2019-12-01) que contiene la información de cada una de las acciones de los clientes del call center.


## ¿Qué permite hacer esta REST API?
Permite traer con el método **GET** dos endpoints que:

 1. Trae un usuario a través de su **clienteId**.
 2. Trea un JSON / Objeto que es un resúmen de las acciones realizadas
    por los clientes  de acuerdo a un rango de días requeridos.

## ¿Qué tecnologías tiene esta REST API?

Esta REST API está hecha en base al entorno NodeJS del lenguaje Javascript, que, dentro de la misma, utiliza las siguientes librerías:

 1. **ExpressJS** : el framework por excelencia de Javascript y NodeJS
 2. **Node-fetch**: librería de uso asíncrono para comunicación con otras API.
 3. **swagger-jsdoc & swagger-ui-express:** dos herramientas que en conjunto posibilitan la documentación de la REST API (más adelante se explica cómo funciona) para evitar crear un archivo HTML.

## ¿Cómo inicio la REST API?

Ya que estás en este repositorio de GIT HUB, puedes descargar el archivo comprimido de este repositorio y guardarlo en tu computadora para probarlo.

Debes entrar a la terminal, sea desde tu GitBash, Powershel o desde el mismo editor de texto; luego de esto, ubicarte en la carpeta Parte1 del repositorio donde luego se instalará las librerías requeridas para la REST API.

Es necesario tener instalado NodeJS en la computadora de prueba y con todo su paquete de instalación de librerías. Si cuenta con esto, debes desde la terminar situandose en la carpeta Parte1, iniciar con el siguiente comando:

    npm install

Luego de esto, verás que se instalará la carpeta node_modules con todas las librerías necesarias para iniciar la REST API. Después, inicias el servidor con el siguiente comando:
 
    node src/index.js 
     
o con nodemon si prefieres, a través de:
 
    nodemon src/index.js 
     
Debes verificar que en la terminal aparezca ***Listen port 3000*** para confirmar que ha iniciado el servidor de la REST API.

Para finalizar, ya se puede navegar en la REST API, desde el siguiente enlace:
[http://localhost:3000/swagger/](http://localhost:3000/swagger/)

## ¿Cómo funciona la REST API?

En la documentación de Swagger, verás que tiene un tag llamado GET, en él verás dos endpoints GET donde podrás ver el  backend de la REST API.

Para probar cada endpoint debes darle click al endpoint a revisar,  y luego darle al botón **Try it out** que está en el costado lateral derecho de los parameters. Luego de esto, verás que se ha habilitado los parameters del endpoint en un listado. Esto se hace para reemplazar el uso en ***query params*** (como se indicó en las indicaciones de la prueba) o es similar a pedirlo a través de la url.

Al suministrar cada información requerida por parameters, debes darle al botón azúl inferior llamado ***Execute***, luego de esto, verá en el apartado de Responses una respuesta ubicada en la parte inferior llamada **Response body** donde verá el JSON con su respuesta.

Dado lo anterior, puede encontrar:

 - [GET. Resumen  cliente:](http://localhost:3000/swagger/#/GET/get_get_resumencliente_dias__dias__id__id_) en este endpoint puede hacer una consulta de un cliente de acuerdo al rango de días que quiera conseguir desde el día 2019-12-01, y el id del cliente que desea traer. En este caso, si ingresas un id que no existe, te traerá una respuesta con estado 400 anunciando que no existe el cliente con el id requerido, si es exitoso, tendrá una respuesta en el **Response body** con un JSON del cliente que requirió.
Si desea, puede ver la  [API](https://apirecruit-gjvkhl2c6a-uc.a.run.app/compras/2019-12-01) donde puede tomar algún clientID, como ID del cliente, o utilizar el clientID ***1941833777***.

 - [GET. Resumen acciones clientes:](http://localhost:3000/swagger/#/GET/get_get_resumen_2019_12_01__dia_)  en este endpoint puede hacer una consulta de un resumen de ventas o acciones de los clientes en la misma [API](https://apirecruit-gjvkhl2c6a-uc.a.run.app/compras/2019-12-01) teniendo en cuenta el rango de días después del reporte del 2019-12-01. Luego de haber habilitado los Parameters con  **Try it out** , ingresas el rango (número) de días que desea revisar desde la fecha mencionada a través de la casilla ahí señalada.  Luego de esto,  al ejecutar la consulta, verá como respuesta en el ***Response body***  un JSON que trae:

	 -  la información del total de transacciones hechas en ese día (ventas efectivas o no).
	 - compras por TDC logradas a través de una función reduce que cuenta todas las formas que hay en ese rango de tiempo.
	 - número de acciones de clientes donde no compraron,  cual se verificó su estado en boolean desde la key **compro**.
	 - la compra más alta del rango de días consultados. 

Has finalizado la revisión de la Prueba 1, gracias por leerme, espero todo haya salido con éxito.

Resuelto por:
Andrés Felipe López Ocho
  Backend Developer Junior 
	 - email: anflopezoc@gmail.com
	 - 3144499986
	 - [LinkedIn](https://www.linkedin.com/in/anflopezoc/)
