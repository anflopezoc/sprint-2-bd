
const express = require('express');
const router = express.Router();
const fetch = require('node-fetch');

async function urls (dias) {
    const allUrl = [];  
    
    for (let i = 1; i <= dias; i++) {
        allUrl.push(`https://apirecruit-gjvkhl2c6a-uc.a.run.app/compras/2019-12-0${i}`)        
    };
    const apiPromise = await allUrl.map(async day => {
        const request = await fetch(day);
        return request.json()
    }); 
    const promise = Promise.all(apiPromise)
    
    return (await promise).flat()
};

function groupTDC(arr, key) {
    const reduceTDC = arr.reduce((acc,el) => {
        if (!acc[el[key]]){ acc[el[key]] = 0;} 
            acc[el[key]] += 1
        return acc
        }, {});
        const ordenTDC = Object.keys(reduceTDC).sort().reduce((obj, key) =>
        { obj[key] = reduceTDC[key]; 
        return obj;},{});
    return ordenTDC
}

//Ver un solo cliente de aceurdo a su clientId en el dia especìfic 
/**
 * @swagger
 * /get/resumencliente/dias/{dias}/id/{id}:
 *      get:    
 *          summary: Devuelve un JSOn con el Cliente con el ID relacionado.
 *          description: A través del Params, se ingresa el numero de dias que desea verificar y el clientID del usuario que desea consultar.
 *          tags: [GET]
 *          parameters:
 *            - in: path
 *              name: dias
 *              description: Número de días
 *              required: true
 *              type: integer 
 *            - in: path
 *              name: id
 *              description: id del cliente
 *              required: true
 *              type: integer    
 *          responses:
 *                  200:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 *                  400:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */

router.get('/resumencliente/dias/:dias/id/:id', async (req, res) => {
    const api= await urls(req.params.dias);
    const cliente = api.find(u => u.clientId == req.params.id)
    if (cliente)  res.status(200).json(cliente)
    else res.status(400).json(`El cliente con id ${req.params.id} no existe`)
});

//Ver resumen en JSON 
/**
 * @swagger
 * /get/resumen/2019-12-01/{dia}:
 *      get:    
 *          summary: Devuelve un JSOn con resumen 
 *          description: A través del Parameters, se requiere el número de días a consultar y devuelve el resumen del día.
 *          tags: [GET]
 *          parameters:
 *            - in: path
 *              name: dia
 *              description: Número de días por consultar
 *              required: true
 *              type: integer    
 *          responses:
 *                  200:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 *                  400:
 *                      content:
 *                          'aplication/json': {}
 *                          'aplication/xml': {}
 */
router.get('/resumen/2019-12-01/:dias', async (req, res) => {
    const api = await urls(req.params.dias);
    const clientsWithMonto = api.filter(u => {if (u.monto !== undefined) return u.monto});
    const montos = clientsWithMonto.map(u => parseInt(u.monto));
    const response = {
        total: api.length,
        comprasPorTDC:  groupTDC(api,'tdc'),
        nocompraron: api.filter(u => u.compro === false).length,
        compraMasAlta: Math.max.apply(null,montos)
    }
    res.json(response)
});


module.exports = router
