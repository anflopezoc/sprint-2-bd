const swaggerOptions = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "Parte 1-Prueba Backend IPCOM",
            version: "1.0.0",
            description: "Prueba sobre una API de Clientes"
        },
        servers: [
            {
                url: "http://localhost:3000",
                description: "Local server"
            }
        ],
        components: {

        },
        security: [

        ]
    },
    apis: ["./src/routes/*.js"]
};

module.exports = swaggerOptions;
