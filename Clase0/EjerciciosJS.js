//Ejercicio 1)
//Enunciado:
//Dada una lista ordenada de números enteros y un valor objetivo buscar si 
//el objetivo se encuentra en la lista y retornar el índice del mismo, si 
//no lo encuentra retornar -1.
//Consideraciones:
//No utilizar funciones de listas/arrays del lenguaje, construir todo de 0.
//Ejemplo 1:
//lista = [1,2,3,4], objetivo = 1, respuesta = 0
//Ejemplo 2:
//lista = [1,2,3,4], objetivo = 10, respuesta = -1

const lista = [1, 2, 3, 4];

function encontrar_indice(lista, objetivo) {
   let l = "";
   for (let i = 0; i < lista.length; i++) {
       if (lista[i] == objetivo){
            l = i+1;
           break;
       };
       if (l == "") l = -1; 
   };
  return console.log(`Resultado E1: ${l}`)

}

//encontrar_indice(lista,5);

//Ejercicio 2)
//Enunciado:
//Dada una lista de números y un valor objetivo retornar los índices de la pareja de números que 
//lleguen a la solución (única).
//Consideraciones:
//Solo hay respuestas únicas ej 10 = 7+3, pero también 6+4. En la lista solo habrá 1 combinación posible. 
//No se puede utilizar más de 1 vez el mismo número (índice), es decir para 10 la solución 5+5 no es válida.
//Ejemplo 1:
//Lista = [1,2,3,4,5], objetivo = 8, respuesta = [2, 4] porque lista[2] = 3, lista[4] = 5 y 5 + 3 = 8

function suma_dos_numeros(lista, objetivo){
    let numX ="";
    let numY = "";
    for (let y = 0; y < lista.length; y++) {
        for (let x = 0; x < lista.length; x++) {
            let a = lista[y] + lista[x];  
            if (a == objetivo && lista[y] !== lista[x] ) { 
                numY = lista[y];                   
                numX = lista[x];
                break
                }
            }  
        }
   if (numX && numY) return console.log(`Resultado E2:: ${numY} y ${numX}.`) 
    else return console.log(`Resultado E2: no hay resultados con el objetivo.`)
}

//suma_dos_numeros(lista,8)


//Ejercicio 3)
//Enunciado:
//Dadas 2 listas ordenadas de números retornar una nueva lista de números 
//que contenga los elementos únicos de ambas lista y que también esté ordenada.
//Consideraciones:
//Las listas iniciales pueden tener elementos repetidos
//Ejemplo 1:
//lista1 = [1,2,3], lista2 = [2,4,5], respuesta = [1,2,3,4,5]
//Ejemplo 2:
//lista1 = [1,2,3,5,7], lista2 = [3,5,7], respuesta = [1,2,3,5,7]
//Ejemplo 3:
//lista1 = [1,1,1,5,7], lista2 = [3,5,7], respuesta = [1,3,5,7]


const lista1 = [1,1,1,5,7];
const lista2 = [3,5,7];

function lista_concatenada_ordenada(lista1,lista2){
    let lista = [];
    let listaOrdenada = [];
    for (let i = 0; i < lista1.length; i++) {
        let comRepetido1 = "";
        for (let j = 0; j < lista.length; j++) {
            if (lista[j] == lista1[i]){
                comRepetido1 = true;
                break
            } else  comRepetido1 = false
        };
        if (comRepetido1 == false ) lista[lista.length] = lista1[i]
    };
    for (let i = 0; i < lista2.length; i++) {
        let comRepetido2 = "";
        for (let j = 0; j < lista.length; j++) {
            if (lista[j] == lista2[i]){
                comRepetido2 = true;
                break
            } else  comRepetido2 = false
        };
        if (comRepetido2 == false ) lista[lista.length] = lista2[i]
    };

    for (let i = 1; i < lista.length; i++) {
    for (let j = 0; j < (lista.length - i); j++) {
        if (lista[j] > lista[j + 1]){
                [lista[j],lista[j + 1]] = [lista[j +1],lista[j]]
        }
        }    
    };
    
    return console.log(lista);
};

//lista_concatenada_ordenada(lista1,lista2)


//Ejercicio 4)
//Enunciado:
//Dado una lista de números donde todos sus elementos aparecen 2 veces retorne 
//el valor del elemento que solo aparece 1 vez
//Consideraciones:
//Las listas iniciales pueden tener elementos repetidos
//Ejemplo 1:
//lista = [1,1,2], respuesta = 2
//Ejemplo 2:
//lista = [4,1,5,1,5], respuesta = 4
//Ejemplo 3:
//lista = [1], respuesta = 1

function no_repetido(filas){
    let num = [];
    for (let i = 0; i < filas.length; i++) {
        let comRepetido = 0;
        for (let j = 0; j < filas.length; j++) {
            if (j !== i ){
                if (filas[j] == filas[i]){
                    comRepetido = 1
                }  
            };
        };
        if (comRepetido == 0) {num[num.length] = filas[i]};
    };
    return console.log(num);
}

//no_repetido([1,5,6,1,5,7])


//Ejercicio 5)
//Enunciado:
//Dado un número de filas retorne todos los elementos del triángulo de pascal hasta esa fila
//Consideraciones:
//Las listas iniciales pueden tener elementos repetidos
//Ejemplo 1:
//filas = 1, respuesta = [[1]]
//Ejemplo 2:
//filas = 2, respuesta = [[1],[1,1]]
//Ejemplo 3:
//filas = 3, respuesta = [[1],[1,1],[1,3,3,1]]

function triangulo_de_pascal(numFilas){
    if(numFilas == 0) return console.log([]);
    if(numFilas == 1) return console.log([[1]]);
    let resultado = []
    for (let i = 1; i <= numFilas; i++) {
        let arr = []
        for (let j = 0; j < i; j++) {
            if (j == 0 || j == i -1) arr[arr.length] = 1            
        else {arr[arr.length] = (resultado[i-2][j-1] + resultado[i-2][j]);}
        }
        resultado[resultado.length] = arr;
    }

    return console.log(resultado);
  
}

//triangulo_de_pascal(8);