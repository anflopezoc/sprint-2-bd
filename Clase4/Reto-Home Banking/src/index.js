const express = require('express');
const app = express();
const mongoose = require('mongoose')
require('./db');

const bankAccount = require('./models/bankAccount_Model');

app.use(express.json());

app.post('/newbankaccount', async (req, res) => {
    const { name, lastName, email } = req.body;
    const emailTrue = await bankAccount.findOne({ email });
    if (emailTrue) {
        res.status(400).json('El correo ya existe');
    } else {
        const newAccountBank = new bankAccount({ name, lastName, email });
        newAccountBank .save();
        res.json(newAccountBank);
    }lastName
});

app.get('/getuserbank', async (req, res) => {
    res.json(await bankAccount.find())
});
app.post('/addmoney/:email', async (req, res) => {
    const { email } = req.params;
    const { money } = req.body;
    const emailTrue = await bankAccount.findOne({ email });
    if (emailTrue) {
        const session = await mongoose.startSession(); 
       session.startTransaction();
       try {
           emailTrue.balance += parseInt(money);
           emailTrue.save();
           await session.commitTransaction();
           session.endSession();
           res.json(emailTrue)
       }
       catch (err){
           await  session.abortTransaction();
           session.endSession();
           res.status(500).json('Internal server error')
       };
        
    } else res.status(400).json('El correo no existe');
    
});

app.post('/transfermoney/:email/to/:emailDest', async (req, res) => {
    const email = req.params.email;
    const emailDest = req.params.emailDest;
    const { money } = req.body;
    const emailTrue = await bankAccount.findOne({ email:email });
    const emailDestTrue = await bankAccount.findOne({ email:emailDest });
    if (emailTrue && emailDestTrue) {
        if(emailTrue.balance >= parseInt(money)){
            const session = await mongoose.startSession(); 
            session.startTransaction();
            try {
                emailTrue.balance -= parseInt(money);
                emailDestTrue.balance += parseInt(money);
                emailTrue.save();
                emailDestTrue.save();
                await session.commitTransaction();
                session.endSession();
                res.json(`${emailTrue.name}${emailTrue.lastName} ha trasnferido ${money} a ${emailDestTrue.name}${emailDestTrue.lastName}`)
            }
            catch (err){
                await  session.abortTransaction();
                session.endSession();
                res.status(500).json('Internal server error')
            };
        } else res.status(400).json('El monto a transferir supera el saldo de la cuenta.');
    } else res.status(400).json('El correo del remitente o del destinatario no existe');
    
});


app.listen(3000);
console.log('Escuchando puerto 3000');