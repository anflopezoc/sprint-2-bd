const mongoose = require('mongoose');

const userBankSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    balance: {
        type: Number,
        default: 0
    },
});

module.exports = mongoose.model('HomeBanking', userBankSchema);