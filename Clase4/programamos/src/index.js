const express = require('express');
const app = express();
require('./db');

const DocumentoPersonal = require('./models/DocumentoPersonal');

app.use(express.json());

app.post('/documentoPersonal', async (req, res) => {
    const { nombre, apellido, email } = req.body;
    const existeDocumento = await DocumentoPersonal.findOne({ email });
    if (existeDocumento) {
        res.status(400).json('El correo ya existe');
    } else {
        const nuevoDocumento = new DocumentoPersonal({ nombre, apellido, email });
        nuevoDocumento.save();
        res.json(nuevoDocumento);
    }
});

app.get('/verContactos', async (req, res) => {
    res.json(await DocumentoPersonal.find())
});
app.post('/agregarContacto/:email', async (req, res) => {
    const { email } = req.params;
    const { nombre, telefono, redesSociales, direccion } = req.body;
    const documentoExistente = await DocumentoPersonal.findOne({ email });
    if (documentoExistente) {
        documentoExistente.contactos.push({ nombre, telefono, redesSociales, direccion });
        documentoExistente.save();
        res.json(documentoExistente);
    } else {
        res.status(400).json('El correo no existe');
    }
});



app.listen(3000);
console.log('Escuchando puerto 3000');