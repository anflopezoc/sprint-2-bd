const mongoose = require('mongoose');

const documentoSchema = new mongoose.Schema({
    nombre: {
        type: String,
        required: true
    },
    apellido: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    contactos: [{
        nombre: String,
        telefono: String,
        redesSociales: String,
        direccion: String
    }]
});

module.exports = mongoose.model('documentoPersonal', documentoSchema);