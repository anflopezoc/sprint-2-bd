const mongoose = require('mongoose');

const ordenSchema = new mongoose.Schema({
    emailUsuario: {
        type: String,
        required: true
    },
    direccion: String,
    productos: [{
        nombre: {
            type: String,
            required: true
        },
        cantidad: {
            type: Number,
            default: 1
        },
        precio: {
            type: Number,
            required: true
        }
    }]
});

module.exports = mongoose.model('Orden', ordenSchema);