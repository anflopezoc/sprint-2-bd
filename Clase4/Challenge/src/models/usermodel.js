const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    user: {type: String, 
        required: true},
    balance: {type: Number, 
        required: true},
    categoria: {type: String, 
        required: true}
  });

const Users = mongoose.model('UserBank', userSchema)
    
module.exports = Users
