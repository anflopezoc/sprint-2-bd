const express = require('express');
const app = express();
const mongoose = require('mongoose');
app.use(express.json());
require('./routes/db')


app.listen('3000', console.log('Listen port 3000'))
