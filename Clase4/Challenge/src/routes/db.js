const express = require('express');
const app = express();
const mongoose = require('mongoose');
const Menu = require('./models/indexProgramamos');
app.use(express.json());

(async () => {
    const db = await mongoose.connect('mongodb://localhost:27017/menu', 
    {useNewUrlParser: true, 
        useUnifiedTopology: true});
    app.get('/obtenermenu', async (req,res)=>{
        const menus = await Menu.find();
        res.json(menus)
    });


    
    app.listen('3000', console.log('Listen port 3000'))
    
})()
