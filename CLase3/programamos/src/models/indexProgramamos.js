const mongoose = require('mongoose');

const menuSchema = new mongoose.Schema({
    plato: {type: String, 
        required: true},
    precio: {type: Number, 
        required: true},
    categoria: {type: String, 
        required: true}
  });

const Menu = mongoose.model('Menu', menuSchema)
    
module.exports = Menu
