const mongoose = require('mongoose');

(async () => {
mongoose.connect('mongodb://localhost:27017/menu', {useNewUrlParser: true, useUnifiedTopology: true});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log('Conectado a la BD')
});

const usersSchema = new mongoose.Schema({
    name: {type: String, 
        required: true},
    lastName: String,
    adress: String,
    edad: Number,
    isAdmin: {
        type: Boolean,
        default:  false
    }
  });

  const User = mongoose.model('Users', usersSchema);

/*   const newUser = new User({name:"Andres", lastName: "Lopez", adress: "Avenida Siempre viva", edad: 30})
  newUser.save();
 */

  console.log(Users);

})()