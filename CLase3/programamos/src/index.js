const express = require('express');
const app = express();
const mongoose = require('mongoose');
const Menu = require('./models/indexProgramamos');
app.use(express.json());

(async () => {
    const db = await mongoose.connect('mongodb://localhost:27017/menu', 
    {useNewUrlParser: true, 
        useUnifiedTopology: true});
    app.get('/obtenermenu', async (req,res)=>{
        const menus = await Menu.find();
        res.json(menus)
    });

    app.post('/crearmenu/', async (req,res) => {
        const {plato, precio, categoria} = req.body;
        const newMenu = await new Menu({plato:plato, precio:precio, categoria:categoria})
        await newMenu.save();
        res.json(await Menu.find())

    });

    app.put('/cambiarmenu/:id', async (req,res) => {
        const menu = await Menu.findById({_id: req.params.id});
        const {plato, precio, categoria} = await req.body;
        menu.plato = plato;
        menu.precio = precio;
        menu.categoria = categoria;
        res.json(menu)
    });

    app.delete('/eliminarmenu/:id', async (req,res) => {
        await Menu.deleteOne({_id: req.params.id});
        res.json(await Menu.find())
    })
    
    app.listen('3000', console.log('Listen port 3000'))
    
})()
