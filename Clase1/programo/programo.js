const fetch = require('node-fetch')
let promesa1 = new Promise((resolve, reject) => {
    setTimeout(()=>{resolve('Hola desde 5 segundos');}, 5000)
})
promesa1
        .then((mensaje) => {console.log(mensaje);})
        .catch((mensaje) => {console.error(mensaje);});

console.log('Hola desde principal');


let promesa2 = new Promise ((resolve, reject) => {
    setTimeout(() =>  {
        resolve('Resuelto en 1 segundo desde promesa 2')
    }, 1000);
});

promesa2
        .then((msg) => {console.log(msg);});
/* 

fetch('https://dog.ceo/api/breeds/image/random')
    .then((res) =>{
        res.json()
        .then((jsonResponse) =>{
            console.log(jsonResponse);
        })
        .catch(() => {
            console.log('error');
        })
    })
    .catch(err => {console.log(err)}) */



    fetch('https://dog.ceo/api/breeds/image/random')
    .then((res) =>{
       return res.json()
    })
    .then((json) =>{
        console.log(json);
    })
    .catch(err => {console.log(err)})