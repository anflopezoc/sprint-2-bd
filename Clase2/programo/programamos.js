const fetch = require('node-fetch');
const express = require('express')
const app = express()

app.use(express.json())

const api_key = "82164e92";
const url = `http://www.omdbapi.com/?apikey=${api_key}&`;

function moviesUrls (movies) {
    const movieTitle = movies.map(u => u.nombrePeli)
    const movieUrl = [];
    for (let i = 0; i < movieTitle.length; i++) {
        movieUrl.push(url + `t=${movieTitle[i]}`)
    }
  return movieUrl
};

function responseMovie (response) {
    return {
        titulo: response.Title,
        imagen: response.Poster,
        descripcion: response.Plot
    }
}

app.post('/movies', async (req, res) => {
    const movieTitle =  moviesUrls(req.body)
    const moviesPromise = await movieTitle.map(async movie => {
            const request = await fetch(movie)
            const movieI = responseMovie(await request.json())
            return movieI
    }); 
    res.json(await Promise.all(moviesPromise))
});



app.listen(3000, console.log('escuchando puerto 3000'))