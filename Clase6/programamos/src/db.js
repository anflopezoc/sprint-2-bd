const express = require('express');
const { Sequelize, Model, DataTypes} = require('sequelize');
const sequelize = new Sequelize('mariadb://root:@localhost:3306/ecommerce');
const app = express();
app.use(express.json());
 
(async function () {
    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
      } catch (error) {
        console.error('Unable to connect to the database:', error);
      }

})();


module.exports = sequelize