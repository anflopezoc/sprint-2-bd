const express = require('express');
const { QueryTypes } = require('sequelize');
const mariadb   = require('mariadb');
const sequelize = require('./db')
const app = express();
app.use(express.json());
 
app.get('/personas', async(req,res) => {
    const response = await sequelize.query('SELECT * FROM persona WHERE esCasado = t',
    { 
        replacements: [1],
        type: QueryTypes.SELECT 
    });
    res.json(response)

})

app.listen('3000'); console.log('listen port 3000')
